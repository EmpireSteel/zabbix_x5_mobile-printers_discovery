import subprocess
import json
import sys


def discovery():
    cmd = ["arp"]
    cmd.append("-a")
    output = subprocess.check_output(cmd).decode('utf-8').split('\n')[:-1]
    devices = []
    for i in output:
        mac = i.split()[3]
        ip = i.split()[1][1:-1]
        if mac.startswith('88:da:1a') or mac.startswith('00:23:a7'):
            devices.append({'model': 'TSC Alpha 3R', 'ip': ip, 'mac': mac})
        elif mac.startswith('ac:3f:a4'):
            devices.append(
                {'model': 'Zebra IMZ22', 'ip': ip, 'mac': mac})
    return devices


def discovery_result(devices):
    result = {"data": [{"{#MPMAC}": dev['mac'], "{#MPMODEL}": dev['model'], "{#MPIP}": dev['ip']} for dev in devices]}
    print(json.dumps(result))


def get_param(devices, mac, param):
    for i in devices:
        if i['mac'] == mac:
            if param == 'model':
                print(i['model'])
            elif param == 'mac':
                print(i['mac'])
            elif param == 'ip':
                print(i['ip'])
            else:
                sys.exit(0)


if __name__ == '__main__':
    devices = discovery()
    if len(sys.argv) == 1:
        discovery_result(devices)
    elif len(sys.argv) == 3:
        mac = sys.argv[1]
        param = sys.argv[2]
        get_param(devices, mac, param)
    else:
        sys.exit(0)
