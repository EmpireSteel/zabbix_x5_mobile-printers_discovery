import subprocess
import json
import sys


def discovery(router_ip):
    sn = 'cacti'
    aw = 'mEgaPasswd'
    pw = '11111111'
    oid = "1.3.6.1.2.1.4.22.1.2"
    sec_name = sn
    auth_proto = "MD5"
    auth_pwd = aw
    priv_proto = "DES"
    priv_pwd = pw
    cmd = ["snmpwalk"]
    cmd.append("-v3")
    cmd.extend(["-l", "authPriv"])
    cmd.extend(["-u", sec_name])
    cmd.extend(["-a", auth_proto])
    cmd.extend(["-A", auth_pwd])
    cmd.extend(["-x", priv_proto])
    cmd.extend(["-X", priv_pwd])
    cmd.extend(["-t", "1"])
    cmd.append(router_ip)
    if oid:
        cmd.append(oid)
    output = subprocess.check_output(cmd).decode('utf-8').split('\n')[:-1]
    devices = []
    for i in output:
        print(i)
        mac = i.split('Hex-STRING: ')[-1]
        ip = '.'.join(i.split(' =')[0].split('.')[-4:])
        if mac.startswith('88 DA 1A') or mac.startswith('00 23 A7'):
            devices.append({'model': 'TSC Alpha 3R', 'ip': ip, 'mac': mac})
        elif mac.startswith('AC 3F A4'):
            devices.append(
                {'model': 'Zebra IMZ22', 'ip': ip, 'mac': mac})
    return devices


def discovery_result(devices):
    result = {"data": [{"{#MAC}": dev['mac'], "{#Model}": dev['model'], "{#Ip}": dev['ip']} for dev in devices]}
    print(json.dumps(result))


def get_param(devices, mac, param):
    for i in devices:
        if i['mac'] == mac:
            if param == 'model':
                print(i['model'])
            elif param == 'mac':
                print(i['mac'])
            elif param == 'ip':
                print(i['ip'])
            else:
                sys.exit(0)


if __name__ == '__main__':
    print(len(sys.argv))
    if len(sys.argv) == 2:
        devices = discovery(sys.argv[1])
        discovery_result(devices)
    elif len(sys.argv) == 4:
        devices = discovery(sys.argv[1])
        mac = sys.argv[2]
        param = sys.argv[3]
        get_param(devices, mac, param)
    else:
        sys.exit(0)
